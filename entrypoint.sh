#!/bin/bash

root=${DB2GIT_ROOT:-"/app"}
default_git_author="db2git"
default_git_email="db2git@example.com"
save_folder="${DB2GIT_DIRECTORY:-${root}/repo}"
git_author=${DB2GIT_GITAUTHOR:-$default_git_author}
git_email=${DB2GIT_GITEMAIL:-$default_git_email}

# You must provide DB2GIT_REPO environment variable with token auth, 
# like this: DB2GIT_REPO="https://token-name:token-value@gitlab.com/user-name/repo-name.git"
if [[ -z $DB2GIT_REPO ]]
then
    echo "ERROR: variable DB2GIT_REPO value not set"
    exit 6
fi

# You must provide MSSQL_SCRIPTER_CONNECTION_STRING environment variable with MS SQL Server connection string
if [[ -z $MSSQL_SCRIPTER_CONNECTION_STRING ]]
then
    echo "ERROR: variable DB2GIT_CONN value not set"
    exit 6
fi

if [[! command -v git &> /dev/null ]]
then
    echo "ERROR: git not installed"
    exit 65
fi

git config --global http.sslVerify false

if [[ ! -d "${save_folder}/.git" ]] 
then
    git clone ${DB2GIT_REPO} ${save_folder}
fi

rm -rf ${save_folder}/*

mssql-scripter -f $save_folder --file-per-object --display-progress  --exclude-headers

cd "${save_folder}"

git config user.name "${git_author}"
git config user.email "${git_email}"
git config --global push.default simple
git add .
git commit -m "autocommit $(date +"%d-%m-%Y %H:%M:%S %z")"
git push
